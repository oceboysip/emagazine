import { createStackNavigator, createAppContainer } from "react-navigation";
import HomeDashboard from "../screen/HomeDashboard";
import DetailScreen from "../screen/DetailScreen";
import pdfview from "../screen/pdfview";



const AppNavigation = createStackNavigator({
  dashboard: {
    screen: HomeDashboard
  },
  detail: {
    screen: DetailScreen
  },
  pdfview: {
    screen: pdfview
  },
  
},

{
  navigationOptions: {
    headerVisible: false,
  },
 });

export default createAppContainer(AppNavigation);
