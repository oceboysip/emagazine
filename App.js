/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
 
import React, {Fragment,Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import AppNavigation from './navigations/AppNavigation';


export default class App extends Component{
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      bookid: null,
   
    }
  }
  
  setBookid = () => {
    this.setState({bookid: bookid})
  }

  render() {
    return <AppNavigation 
        screenProps={{
            ...this.state,
            setBookid: this.setBookid,
            
        }}
      />
  }
}

