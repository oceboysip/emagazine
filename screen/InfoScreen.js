import React, {Component} from 'react';
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import InfoStyle from '../style/InfoStyle';

export default class InfoScreen extends Component {
  render() {
    return (
      <ImageBackground source={require('../images/bg-dashboard2.jpg')} style={{width: '100%', height: '100%'}}>
      <View>
        <View style={styles.navTop}>
          <Image style={{width:150, height:10,resizeMode:'contain'}} source={require('../images/logo.png')}/>
        </View>

        <View style={styles.container}>
           <View style={styles.rowSection}>
              <View style={styles.borderBot}>
                <Text style={styles.bigText}>DIKNAS E-MAGAZINE APPS</Text>
              </View>
              <Text style={styles.textSmall}>Version{"\n"}1.0.0{"\n"}Last update{"\n"}Juli 2019</Text>
              <Text style={styles.textMiddle}>{'\u00A9'} Emagazine 2018-2019</Text>
           </View>
        </View>
      </View>

      </ImageBackground>



    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(InfoStyle);