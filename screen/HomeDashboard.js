import React, {Component} from 'react';
import {Alert,Platform, StyleSheet,AsyncStorage, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import DashboardStyle from '../style/DashboardStyle';
import axios from 'axios';
import Carousel from 'react-native-banner-carousel';
import {constant} from '../constants';

class HomeDashboard extends Component {

  constructor(props){
    super(props)
    this.state = {
      user: null,
      buku:[],
      tempbook:[],
      tempheadline:[],
      headline:[],
      temppopular:[],
      popular:[],
      date:''
      
    };
  }

  componentDidMount(){
   
    this._loadInitialState().done();
   
    var that = this;
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun","Jul", "Agu", "Sep", "Okt", "Nov", "Des"];
    
    var date = new Date().getDate();
    var month = monthNames[new Date().getMonth()];
    var year = new Date().getFullYear();
    that.setState({
      date:
         date + '-' + month + '-' + year,
      banner:[]
    });
    this.timeoutHandle = setTimeout(()=>{
  
 }, 2000);
    
  }

  componentWillUnmount(){
    clearTimeout(this.timeoutHandle); 
}

  _loadInitialState = async () => {
    this._loadbook();
  }


  _loadbook = () => {
    axios({
      method: 'get',
      url: constant.api_url+"book/"
    })
    .then((response) => {
        res = response.data;
        console.log(res.data)
        //Lastest Release
        this.setState({tempbook:res.data.lastest});
        //Recommend To you
        this.setState({tempheadline:res.data.headline});
        //Popular
        this.setState({temppopular:res.data.populer});
        
    })
    .catch(function (error) {
      console.log('masuk'+error)
    });
  }
  detail = () => {
    this.props.navigation.navigate('detail');
  }


  renderBook(image, index, judul, penulis){
    return   (<View style={styles.bookCont}>
      <TouchableOpacity style={styles.coverImg} onPress={this.detail}>
        <Image style={styles.mainImg} source={{ uri: image }}/>
      </TouchableOpacity>
    <View style={styles.textCover}>
        <Text style={styles.judulMag}>{judul}</Text>
        <Text style={styles.penulis}>{penulis}</Text>
    </View>
</View>);
  }

 
  renderHeadline(image, index, judul, penulis){
    return   (<View style={styles.bookContSmall}>
      <TouchableOpacity style={styles.coverImgSmall} onPress={this.detail}>
        <Image style={styles.mainImg} source={{ uri: image }}/>
      </TouchableOpacity>
    <View style={styles.textCover}>
        <Text style={styles.judulMag}>{judul}</Text>
        <Text style={styles.penulis}>{penulis}</Text>
    </View>
</View>);

  }

  renderPopular(image, index, judul, penulis){
    return   (<View style={styles.bookContSmall}>
      <TouchableOpacity style={styles.coverImgSmall} onPress={this.detail}>
        <Image style={styles.mainImg} source={{ uri: image }}/>
      </TouchableOpacity>
    <View style={styles.textCover}>
        <Text style={styles.judulMag}>{judul}</Text>
        <Text style={styles.penulis}>{penulis}</Text>
    </View>
</View>);

  }

  render() {
    return (
      <ImageBackground source={require('../images/bg-dashboard.jpg')} style={{width: '100%', height: '100%'}}>
      <ScrollView>
        <View style={styles.navTop}>
          <Image style={{width:150, height:10,resizeMode:'contain'}} source={require('../images/logo.png')}/>
          <Text>.:: Tgl : {this.state.date} ::.</Text>
        </View>
        <View style={styles.container}>
          <View style={styles.rowSection}>
            <Text style={styles.judulBig}>Latest Releases </Text>
            <View style={styles.contSrcroll}>
              <ScrollView 
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                >
                  {this.state.tempbook.map((image, index) => this.renderBook(image.images, index, image.judul, image.penulis))}
              </ScrollView>
            </View>
          </View>


          <View style={styles.rowSection}>
            <View style={{position:'relative'}}>
              <TextInput
                placeholder="Enter the name or author of the book"
                style={styles.inputText}
              />
              <Image style={{width: 25, height: 26, resizeMode: 'contain',position:'absolute',top:10,left:10,}} source={require('../images/icon_search.png')}/>
            </View>
          </View>

          <View style={styles.rowSection}>
            <Text style={styles.judulSmall}>Recommended </Text>
            <View style={styles.contSrcroll}>
              <ScrollView 
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                >


              {this.state.tempheadline.map((image, index) => this.renderHeadline(image.images, index, image.judul, image.penulis))}
              

              
              </ScrollView>
            </View>
          </View>


          <View style={styles.rowSection}>
            <Text style={styles.judulSmall}>Popular This Week </Text>
            <View style={styles.contSrcroll}>
              <ScrollView 
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                >


{this.state.temppopular.map((image, index) => this.renderPopular(image.images, index, image.judul, image.penulis))}
              
              </ScrollView>
            </View>
          </View>

        </View>
        </ScrollView>

        <View style={styles.footer}>
          <View style={styles.boxShadow2}>
            <View style={styles.boxInner}>
              <View style={styles.rowButton}>
                <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={require('../images/icon_emagazine.png')}/>
                <Text style={styles.textButton2}>E-magazine</Text>
              </View>

         
              <View style={styles.rowButton}>
                <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={require('../images/icon_home.png')}/>
                <Text style={styles.textButton2}>Home</Text>
              </View>

              <View style={styles.rowButton}>
                <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={require('../images/icon_einfo.png')}/>
                <Text style={styles.textButton2}>E-Info</Text>
              </View>
            
            </View>
          </View>
        </View>

      </ImageBackground>



    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(DashboardStyle);

export default HomeDashboard;