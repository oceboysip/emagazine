import React, {Component} from 'react';
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import SearchStyle from '../style/SearchStyle';
import Carousel from 'react-native-banner-carousel';

export default class SearchScreen extends Component {
  detail = () => {
    this.props.navigation.navigate('detail');
  }
  render() {
    return (
      <ImageBackground source={require('../images/bg-dashboard.jpg')} style={{width: '100%', height: '100%'}}>
      <ScrollView>
        <View style={styles.navTop}>
          <Image style={{width:150, height:10,resizeMode:'contain'}} source={require('../images/logo.png')}/>
        </View>
        <View style={styles.container}>
          <View style={styles.rowSection}>
            <View style={{position:'relative'}}>
              <TextInput
                placeholder="Enter the name or author of the book"
                style={styles.inputText}
              />
              <Image style={{width: 25, height: 26, resizeMode: 'contain',position:'absolute',top:10,left:10,}} source={require('../images/icon_search.png')}/>
            </View>
          </View>

          <View style={styles.rowSection}>
            <Text style={styles.judulSmall}>Hasil pencarian "testing .." </Text>
            <View style={styles.rowBook}>
              <View style={styles.rows2}>
                <View style={styles.bookContSmall}>
                  <View style={styles.coverImgSmall}>  
                    <Image style={styles.mainImg} source={require('../images/cover_small2.jpg')}/>
                  </View>
                  <View style={styles.textCover}>
                      <Text style={styles.judulMagSmall}>Lorem Ipsum dolor sit</Text>
                      <Text style={styles.penulisSmall}>Erik Slavin</Text>
                  </View>
                </View>
              </View>

              <View style={styles.rows2}>
                <View style={styles.bookContSmall}>
                  <View style={styles.coverImgSmall}>  
                    <Image style={styles.mainImg} source={require('../images/cover_small2.jpg')}/>
                  </View>
                  <View style={styles.textCover}>
                      <Text style={styles.judulMagSmall}>Lorem Ipsum dolor sit</Text>
                      <Text style={styles.penulisSmall}>Erik Slavin</Text>
                  </View>
                </View>
              </View>

              <View style={styles.rows2}>
                <View style={styles.bookContSmall}>
                  <View style={styles.coverImgSmall}>  
                    <Image style={styles.mainImg} source={require('../images/cover_small2.jpg')}/>
                  </View>
                  <View style={styles.textCover}>
                      <Text style={styles.judulMagSmall}>Lorem Ipsum dolor sit</Text>
                      <Text style={styles.penulisSmall}>Erik Slavin</Text>
                  </View>
                </View>
              </View>

              <View style={styles.rows2}>
                <View style={styles.bookContSmall}>
                  <View style={styles.coverImgSmall}>  
                    <Image style={styles.mainImg} source={require('../images/cover_small2.jpg')}/>
                  </View>
                  <View style={styles.textCover}>
                      <Text style={styles.judulMagSmall}>Lorem Ipsum dolor sit</Text>
                      <Text style={styles.penulisSmall}>Erik Slavin</Text>
                  </View>
                </View>
              </View>

              <View style={styles.rows2}>
                <View style={styles.bookContSmall}>
                  <View style={styles.coverImgSmall}>  
                    <Image style={styles.mainImg} source={require('../images/cover_small2.jpg')}/>
                  </View>
                  <View style={styles.textCover}>
                      <Text style={styles.judulMagSmall}>Lorem Ipsum dolor sit</Text>
                      <Text style={styles.penulisSmall}>Erik Slavin</Text>
                  </View>
                </View>
              </View>
            </View>
          </View>


        </View>
        </ScrollView>

        <View style={styles.footer}>
          <View style={styles.boxShadow2}>
            <View style={styles.boxInner}>
              <View style={styles.rowButton}>
                <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={require('../images/icon_emagazine.png')}/>
                <Text style={styles.textButton2}>E-magazine</Text>
              </View>

              <View style={styles.rowButton}>
                <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={require('../images/icon_video.png')}/>
                <Text style={styles.textButton2}>Video</Text>
              </View>

              <View style={styles.rowButton}>
                <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={require('../images/icon_home.png')}/>
                <Text style={styles.textButton2}>Home</Text>
              </View>

              <View style={styles.rowButton}>
                <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={require('../images/icon_einfo.png')}/>
                <Text style={styles.textButton2}>E-Info</Text>
              </View>
            
            </View>
          </View>
        </View>

      </ImageBackground>



    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(SearchStyle);