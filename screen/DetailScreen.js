import React, {Component} from 'react';
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import DetailStyle from '../style/DetailStyle';
import Carousel from 'react-native-banner-carousel';

export default class DetailScreen extends Component {
  dashboard = () => {
    this.props.navigation.navigate('dashboard');
  }

  detailpdf = () => {
    this.props.navigation.navigate('pdfview');
  }
  render() {
    return (
      <View>
      <ScrollView>
        <View style={styles.navTop}>
          <TouchableOpacity onPress={this.dashboard}>
            <Image style={{width:40, height:28,resizeMode:'contain'}} source={require('../images/arrow_back.png')}/>
          </TouchableOpacity>
        </View>
        <View style={styles.container}>

          <View style={styles.rowSection}>
            <View style={{marginBottom:0}}>
              <Text style={styles.judulMag}>Lorem Ipsum dolor sit</Text>
              <Text style={styles.penulis}>Erik Slavin</Text>
            </View>

            <View style={styles.coverCenter}>
              <Image style={{width: 350, height: 449, resizeMode: 'contain'}} source={require('../images/big_cover.png')}/>
              <View style={{flexDirection:'row'}}>
                <TouchableOpacity style={{marginHorizontal:5}} onPress={this.detailpdf}>
                    <Image style={{width:100, height:33,resizeMode:'contain'}} source={require('../images/bt_read.png')}/>
                </TouchableOpacity>
                <TouchableOpacity style={{marginHorizontal:5}}>
                    <Image style={{width:100, height:33,resizeMode:'contain'}} source={require('../images/bt_share.png')}/>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={styles.rowSection}>
            <View style={{marginBottom:0}}>
              <Text style={styles.judulMag}>Book Description</Text>
               <View style={{flexDirection:'row'}}>
                <Text style={styles.catText}>History</Text>
               </View>
               <Text style={styles.textIsi}>
                  Europe has for two millennia been a remarkably successful continent. In this dazzling new history, bestselling author Simon Jenkins tells the story of its evolution from a battlefield of warring tribes to peace, wealth and freedom - a story that twists and turns from Greece and Rome, through the Middle Ages, Reformation and French Revolution, to the two World Wars and the present day.
                </Text>
            </View>
          </View>

          <View style={styles.rowSection}>
            <Text style={styles.judulSmall}>More by Simon Jenkins </Text>
            <View style={styles.contSrcroll}>
              <ScrollView 
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                >
                  <View style={styles.bookContSmall}>
                    <View style={styles.coverImgSmall}>  
                      <Image style={styles.mainImg} source={require('../images/cover_small1.jpg')}/>
                    </View>
                    <View style={styles.textCover}>
                        <Text style={styles.judulMagSmall}>Lorem Ipsum dolor sit</Text>
                        <Text style={styles.penulisSmall}>Erik Slavin</Text>
                    </View>
                </View>

                <View style={styles.bookContSmall}>
                    <View style={styles.coverImgSmall}>  
                      <Image style={styles.mainImg} source={require('../images/cover_small2.jpg')}/>
                    </View>
                    <View style={styles.textCover}>
                        <Text style={styles.judulMagSmall}>Lorem Ipsum dolor sit</Text>
                        <Text style={styles.penulisSmall}>Erik Slavin</Text>
                    </View>
                </View>
                <View style={styles.bookContSmall}>
                    <View style={styles.coverImgSmall}>  
                      <Image style={styles.mainImg} source={require('../images/cover_small1.jpg')}/>
                    </View>
                    <View style={styles.textCover}>
                        <Text style={styles.judulMagSmall}>Lorem Ipsum dolor sit</Text>
                        <Text style={styles.penulisSmall}>Erik Slavin</Text>
                    </View>
                </View>

                <View style={styles.bookContSmall}>
                    <View style={styles.coverImgSmall}>  
                      <Image style={styles.mainImg} source={require('../images/cover_small2.jpg')}/>
                    </View>
                    <View style={styles.textCover}>
                        <Text style={styles.judulMagSmall}>Lorem Ipsum dolor sit</Text>
                        <Text style={styles.penulisSmall}>Erik Slavin</Text>
                    </View>
                </View>
              </ScrollView>
            </View>
          </View>

          <View style={styles.rowSection}>
            <Text style={styles.judulSmall}>Similar Books </Text>
            <View style={styles.contSrcroll}>
              <ScrollView 
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                >
                  <View style={styles.bookContSmall}>
                    <View style={styles.coverImgSmall}>  
                      <Image style={styles.mainImg} source={require('../images/cover_small1.jpg')}/>
                    </View>
                    <View style={styles.textCover}>
                        <Text style={styles.judulMagSmall}>Lorem Ipsum dolor sit</Text>
                        <Text style={styles.penulisSmall}>Erik Slavin</Text>
                    </View>
                </View>

                <View style={styles.bookContSmall}>
                    <View style={styles.coverImgSmall}>  
                      <Image style={styles.mainImg} source={require('../images/cover_small2.jpg')}/>
                    </View>
                    <View style={styles.textCover}>
                        <Text style={styles.judulMagSmall}>Lorem Ipsum dolor sit</Text>
                        <Text style={styles.penulisSmall}>Erik Slavin</Text>
                    </View>
                </View>
                <View style={styles.bookContSmall}>
                    <View style={styles.coverImgSmall}>  
                      <Image style={styles.mainImg} source={require('../images/cover_small1.jpg')}/>
                    </View>
                    <View style={styles.textCover}>
                        <Text style={styles.judulMagSmall}>Lorem Ipsum dolor sit</Text>
                        <Text style={styles.penulisSmall}>Erik Slavin</Text>
                    </View>
                </View>

                <View style={styles.bookContSmall}>
                    <View style={styles.coverImgSmall}>  
                      <Image style={styles.mainImg} source={require('../images/cover_small2.jpg')}/>
                    </View>
                    <View style={styles.textCover}>
                        <Text style={styles.judulMagSmall}>Lorem Ipsum dolor sit</Text>
                        <Text style={styles.penulisSmall}>Erik Slavin</Text>
                    </View>
                </View>
              </ScrollView>
            </View>
          </View>

        </View>
        </ScrollView>

        <View style={styles.footer}>
          <View style={styles.boxShadow2}>
            <View style={styles.boxInner}>
              <View style={styles.rowButton}>
                <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={require('../images/icon_emagazine.png')}/>
                <Text style={styles.textButton2}>E-magazine</Text>
              </View>

              <View style={styles.rowButton}>
                <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={require('../images/icon_video.png')}/>
                <Text style={styles.textButton2}>Video</Text>
              </View>

              <View style={styles.rowButton}>
                <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={require('../images/icon_home.png')}/>
                <Text style={styles.textButton2}>Home</Text>
              </View>

              <View style={styles.rowButton}>
                <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={require('../images/icon_einfo.png')}/>
                <Text style={styles.textButton2}>E-Info</Text>
              </View>
            
            </View>
          </View>
        </View>

      </View>



    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(DetailStyle);