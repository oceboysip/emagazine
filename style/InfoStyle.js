import Dimensions from 'react-native';

export default InfoStyle = {
    container:{
      flex:1,
    },
    navTop:{
      height:50,
      paddingHorizontal:20,
      paddingTop:20,
      textAlign:'center',
      alignItems:'center',  
    },
    rowSection:{
      paddingHorizontal:15,
      paddingVertical:15,
      borderRadius:5,
      marginBottom:10,
      marginHorizontal:5,
      position:'relative',
    },
    judulBig:{
      fontSize:22,
      color:'#fff',
      fontWeight:'bold',
    },
    judulSmall:{
      fontSize:20,
      color:'#878787',
      fontWeight:'bold',
    },
    contSrcroll:{
      marginTop:15
    },
    judulMag:{
      fontSize:20,
      color:"#878787",
      fontWeight:'bold'
    },
    judulMagSmall:{
      fontSize:17,
      color:"#878787",
      fontWeight:'bold'

    },
    penulis:{
      fontSize:17,
      color:"#a5a5a5"
    },
    penulisSmall:{
      fontSize:14,
      color:"#a5a5a5"
    },

    inputText:{
      fontSize:15,
      alignSelf:'stretch',
      borderColor:'#ddd',
      borderWidth:1,
      borderRadius:10,
      paddingLeft:45,
      backgroundColor:'#fff'
    },
    bookCont:{
      width: 300, marginRight: 15  
    },
    bookContSmall:{
      width: 150, marginRight: 15  
    },
    coverImg:{
      height:378
    },
    coverImgSmall:{
      height:217
    },
    mainImg:{
      flex: 1, width: null, height: null, resizeMode: 'cover',borderRadius:10
    },
    textCover:{
      flex: 2, paddingLeft: 10, paddingTop: 10
    },
    footer:{
      height:55,
      backgroundColor:'#FFFFFF',
      borderColor:'#ddd',
      borderTopWidth:2
    },
    boxShadow2:{
      shadowColor: "#000",
      shadowOffset: {
      width: 0,
      height: 7,
      },
      shadowOpacity: 0.41,
      shadowRadius: 9.11,
      elevation: 14,
    },
    boxInner:{
      flexDirection:'row',
      paddingHorizontal:5,
      backgroundColor:'#fff',
      paddingVertical:5,
      paddingBottom:0 
    },
    rowButton:{
      flex:1,
      alignItems:'center',
      justifyContent:'center',
    },
    textButton2:{
      color:'#a9a9a9',
      fontSize:10,
      fontWeight:'bold',
      marginTop:0,
    },
     bigText:{
      fontSize:28,
      color:'#fff',
      fontWeight:'bold',
      paddingBottom:5,
    },
    borderBot:{
      width:240,
      borderBottomColor:'#fff',
      borderBottomWidth: 2,
      marginBottom:5,
      height:120
    },
    textSmall:{
      fontSize:14,
      color:'#fff',
      marginBottom:5,
    },
    textMiddle:{
      fontSize:17,
      color:'#fff',
    },
    


  }